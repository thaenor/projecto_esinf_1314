#ifndef AutoEstrada_
#define AutoEstrada_

class AutoEstrada: public Via
{
private:
	double portagem; /* Pre�o da Portagem da Via */
public:
	AutoEstrada(); /* Construtor por Defeito */
	AutoEstrada(const string codigo, int total_km, int tempo_medio,double portagem, const Local &local_origem, const Local &local_destino); /* Construtor com parametros */
	AutoEstrada(const AutoEstrada&a); /* Construtor Copia */
	~AutoEstrada(); /* Destrutor */ 

	AutoEstrada* clone()const;/* Clone */
	void listar() const; /* M�todo Listar */
	
	/* M�todos de Acesso (get's) */
	virtual double getCusto() const;
	/* M�todos Modificadores de Acesso (set's) */
    void setPortagem(double portagem);

	// => Sobrecarga de Operadores

		void escreve(ostream &out) const;

	// Declara��o da sobrecarga do operador de atribui��o na classe Natural
	
		AutoEstrada & operator = ( const AutoEstrada &a);
};

/* Construtor por Defeito */
AutoEstrada::AutoEstrada():Via(){
    portagem = 0;
}

/* Construtor por parametros */
AutoEstrada::AutoEstrada(const string codigo, int total_km, int tempo_medio,double portagem, const Local &local_origem, const Local &local_destino):Via(codigo, total_km, tempo_medio,local_origem,local_destino)
{
	this->portagem = portagem;
}

/* Construtor Copia */
AutoEstrada::AutoEstrada(const AutoEstrada &a):Via(a){
     this->portagem=a.portagem;
}

/* Destrutor */
AutoEstrada::~AutoEstrada()
{
}

/* M�todo Clone */
AutoEstrada* AutoEstrada::clone()const
{
    return new AutoEstrada(*this);
}
/* M�todos Modificadores de Acesso (set) */

void AutoEstrada::setPortagem(double p)  {
    portagem = p;
}
/* M�todos de Acesso (get) */

double AutoEstrada::getCusto() const {
    return portagem;
}

/* M�todo Listar */
void AutoEstrada::listar() const
{
	Via::listar();
	cout << "Preco da Portagem: " << portagem << endl;
}

void AutoEstrada::escreve(ostream &out) const
{
	Via::escreve(out);
	out << "Preco da Portagem: " << portagem << endl;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const AutoEstrada &a){
	a.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const AutoEstrada *a){
	a->escreve(out);
	return out;
}

// Implementa��o  do operador de atribui��o na classe AutoEstrada
AutoEstrada & AutoEstrada::operator = ( const AutoEstrada &a){
	if(this!=&a)
	{Via::operator=(a);
	portagem=a.portagem;
	}
	return *this;
}

#endif