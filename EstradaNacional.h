#ifndef EstradaNacional_
#define EstradaNacional_

class EstradaNacional: public Via
{
private:
	string tipo_pavimento; /* Tipo de Pavimento -> exemplo: asfalto */
public:
	EstradaNacional();/* Construtor por defeito */
	EstradaNacional(const string codigo, int total_km, int tempo_medio, const string tipo_pavimento, const Local &local_origem, const Local &local_destino); /* Construtor com parametros */
	EstradaNacional(const EstradaNacional&en); /* Construtor copia */
	~EstradaNacional(); /* Destrutor */ 

	EstradaNacional* clone()const; /* Clone */
	void listar() const; /* M�todo Listar */

	/* M�todos de Acesso (get's) */
	string getTipo_pavimento();

	/* M�todos Modificadores de Acesso (set's) */
	void setTipo_pavimento(string pavimento);

	// => Sobrecarga de Operadores

		void escreve(ostream &out) const;

	// Declara��o da sobrecarga do operador de atribui��o na classe EstradaNacional
	
		EstradaNacional & operator = ( const EstradaNacional &en);
};

/* Construtor por Defeito */
EstradaNacional::EstradaNacional():Via(){
    tipo_pavimento = "";
}

/* Construtor por parametros */
EstradaNacional::EstradaNacional(const string codigo, int total_km, int tempo_medio, const string tipo_pavimento, const Local &local_origem, const Local &local_destino):Via(codigo,total_km,tempo_medio,local_origem,local_destino)
{
	this->tipo_pavimento = tipo_pavimento;
}

/* Construtor Copia */
EstradaNacional::EstradaNacional(const EstradaNacional &en):Via(en){
    this->tipo_pavimento=en.tipo_pavimento;
}

/* Destrutor */
EstradaNacional::~EstradaNacional()
{
}

/* M�todo Clone */
EstradaNacional* EstradaNacional::clone()const
{
    return new EstradaNacional(*this);
}

/* M�todos Modificadores de Acesso (set) */

void EstradaNacional::setTipo_pavimento(string pavimento)  {
    tipo_pavimento = pavimento;
}
/* M�todos de Acesso (get) */

string EstradaNacional::getTipo_pavimento() {
    return tipo_pavimento;
}

/* M�todo Listar */
void EstradaNacional::listar() const
{
	Via::listar();
	cout << "Tipo de Pavimento: " << tipo_pavimento << endl;
}

void EstradaNacional::escreve(ostream &out) const
{
	Via::escreve(out);
	out << "Tipo de Pavimento: " << tipo_pavimento << endl;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const EstradaNacional &en){
	en.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const EstradaNacional *en){
	en->escreve(out);
	return out;
}

// Implementa��o  do operador de atribui��o na classe EstradaNacional
EstradaNacional & EstradaNacional::operator = ( const EstradaNacional &en){
	if(this!=&en)
	{Via::operator=(en);
	tipo_pavimento=en.tipo_pavimento;
	}
	return *this;
}

#endif