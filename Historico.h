#ifndef Historico_
#define Historico_

class Historico: public Local
{
private:
	int tempo_visita; /* tempo m�dio de visita do local (em minutos) */
	int horario_abertura; /* Horario de Abertura (em minutos) */
	int horario_encerramento;/* Horario de Encerramento (em minutos) */

public:
	Historico(); /* Construtor por Defeito */
	Historico(const string descricao, int tempo_visita, int horario_abertura, int horario_encerramento); /* Construtor com parametros */
	Historico(const Historico&h);/* Construtor copia */
	~Historico(); /* Destrutor */
	
	Historico* clone()const; /* Clone */

		 void listar() const; /* M�todo Listar */
	
		 void escreve(ostream &out) const;

	// => Sobrecarga de Operadores

	// Declara�ao do operador de atribui��o na classe Historico
		 Historico & operator = ( const Historico &h);

	/* M�todos de Acesso (get's) */
	int getTempoVisita();
	int getHorario_abertura();
	int getHorario_encerramento();

	/* M�todos Modificadores de Acesso (set's) */
	void setTempoVisita(int tempo_min);
	void setHorario_abertura(int abre);
	void setHorario_encerramento(int fecha);

};
/* Construtor por Defeito */
Historico::Historico():Local(){
    tempo_visita = 0;
	horario_abertura=0;
	horario_encerramento=0;
}

/* Construtor com parametros */
Historico::Historico(const string descricao, int tempo_visita, int horario_abertura, int horario_encerramento):Local(descricao){

	this->tempo_visita=tempo_visita;
	this->horario_abertura=horario_abertura;
	this->horario_encerramento=horario_encerramento;
}

/* Construtor Copia */
Historico::Historico(const Historico &h):Local(h){
    this->tempo_visita=h.tempo_visita;
	this->horario_abertura=h.horario_abertura;
	this->horario_encerramento=h.horario_encerramento;
}

/* Destrutor */
Historico::~Historico()
{
}

/* M�todo Clone */
Historico* Historico::clone()const
{
    return new Historico(*this);
}

/* M�todos de Acesso (get's) */

int Historico::getTempoVisita(){
    return tempo_visita;
}

int Historico::getHorario_abertura(){
    return horario_abertura;
}

int Historico::getHorario_encerramento(){
    return horario_encerramento;
}

/* M�todos Modificadores de Acesso (set's) */

void Historico::setTempoVisita(int tempo_min){
    tempo_visita = tempo_min;
}

void Historico::setHorario_abertura(int abre){
    horario_abertura = abre;
}

void Historico::setHorario_encerramento(int fecha){
    horario_encerramento = fecha;
}

/* M�todo Listar */
void Historico::listar() const
{
	Local::listar();
	cout << "Tempo medio de visita do local: " << tempo_visita <<endl;
	cout << "Horario de Abertura: " << horario_abertura << endl;
	cout << "Horario de Encerramento: " << horario_encerramento << endl;
	
}

void Historico::escreve(ostream &out) const
{
	Local::escreve(out);
	out << "Tempo medio de visita do local: " << tempo_visita <<endl;
	out << "Horario de Abertura: " << horario_abertura << endl;
	out << "Horario de Encerramento: " << horario_encerramento << endl;

}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Historico &h){
	h.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Historico *h){
	h->escreve(out);
	return out;
}

// Implementa��o do operador de atribui��o na classe Historico
Historico & Historico::operator = ( const Historico &h){
	if(this!=&h)
	{Local::operator=(h);
	tempo_visita=h.tempo_visita;
	horario_abertura=h.horario_abertura;
	horario_encerramento=h.horario_encerramento;
	}
	return *this;
}
#endif