#ifndef Local_
#define Local_

class Local /* SuperClasse */
{
	private:
		string descricao; /* Descri��o do Local de interesse tur�stico */

	public:
		Local(); /* Construtor por Defeito */
		Local(const string descricao); /* Construtor com parametros */
		Local(const Local &local); /* Construtor copia */
		virtual ~Local(); /* Destrutor */ 

		/* M�todos Modificadores de Acesso (set) */
		
		void setDescricao(string descricao);

		/* M�todos de Acesso (get) */

		string getDescricao() const;

		virtual Local* clone()const; /* Clone */
		virtual void listar() const;/* M�todo para Listar descri��o do Local */

// => Sobrecarga de Operadores

	virtual void escreve(ostream &out) const;

	// Declara��o da sobrecarga do operador de atribui��o na classe Local
	virtual Local & operator = (const Local &l);

	// Declara��o da sobrecarga do operador > que permita comparar o a decricao de dois locais
	bool operator > ( const Local &l) const; // Compara��o pela Descri��o do Local

	// Declara��o da sobrecarga  do operador == que compare a descricao de dois locais.

	bool operator==(const Local& l) const;
};

/* Construtor por Defeito */
Local::Local()
{
	descricao="";
};

/* Construtor com parametros */
Local::Local(string descricao)
{
	this->descricao=descricao;
}

/* Construtor copia */
Local::Local(const Local& l) {
	descricao=l.descricao;
}

/* Destrutor */ 
Local::~Local() {

}

/* M�todos Modificadores de Acesso (set) */

void Local::setDescricao(string desc)  {
    descricao = desc;
}

/* M�todos de Acesso (get) */

string Local::getDescricao() const {
    return descricao;
}

/* M�todo Clone */

Local* Local::clone()const
{
    return new Local(*this);
}

/* M�todo Listar -> Lista Conteudo do Objecto da classe Local*/
void Local::listar() const
{
	cout << "Descricao do local turistico: " << descricao;
}

void Local::escreve(ostream &out) const
{
	//out <<  "Descricao do local turistico: " << descricao << endl;
	out << descricao << endl;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Local &l){
	l.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Local *l){
	l->escreve(out);
	return out;
}

// Implementa��o da sobrecarga do operador == que compara descricao de dois locais.

bool Local::operator==(const Local& l) const
{
	if (descricao == l.descricao)
		return true;
	else
		return false;
}


// Implementa��o da sobrecarga do operador de atribui��o na classe Local
Local & Local::operator = (const Local &l){
	if(&l!=this){
		descricao=l.descricao;
	}
	return *this;
}

// Implementa��o da sobrecarga do operador > que permita comparar o n�mero de duas p�ginas.
bool Local::operator > ( const Local &l) const{
	return (descricao > l.descricao);
}

#endif
