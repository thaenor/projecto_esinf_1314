// Ficheiros do Sistema
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <fstream> // stream dos ficheiros da biblioteca STO

using namespace std;// todas funcionalidades da iostream

// Header Files
// -> primeiro o include das classes bases, depois as classes derivadas

#include "Local.h"

	#include "Natural.h"
	#include "Historico.h"

#include "Via.h"

	#include "EstradaNacional.h"
	#include "AutoEstrada.h"

#include "PVia.h"
#include "Mapa.h"

#include "Teste.h"

// Criacao de menus
	void menuprincipal();
	void menusecundario(Teste &test);
	void menusecundario2(Teste &test,Mapa &map); // Acrescentado

//Funções para compatibilidade entre Sistemas Operativos
void clear()
{
#ifdef _WIN64
    system("cls");
#elif _WIN32
    system("cls");
#else
    cout << string( 100, '\n' );
#endif
    
}

void pause()
{
#ifdef _WIN64
    system("pause");
#elif _WIN32
    system("pause");
#else
    cout << "paused, press any key to resume.";
    cin.get();
    cin.get();
#endif
}

//Funcao main

int main(){
	menuprincipal();
    return 0;
}
void menusecundario2(Teste &test,Mapa &map){
	bool flag=false;
	int opcao;
	do{
		cout << " Trabalho Pratico - ESINF 2013 \n" << endl;
		cout<<endl<<endl<<endl;
		cout << " |_______________ MENU______________"<<endl;
		cout << " 1: Carregar Grafo"<<endl;
		cout << " 2: Visita em Largura"<<endl;
		cout << " 3: Visita em Profundidade "<<endl;
		cout << " 4: Caminhos Distintos "<<endl;
        cout << " 5: Caminho mais curto "<<endl;
        cout << " 6: Caminho mais economico "<<endl;
		cout << " 7: De maior interesse turístico " <<endl;
		cout << " 0: Voltar ao Menu Principal "<<endl;
		cout << " |__________________________________|"<<endl;
		cin>>opcao;
		switch (opcao){
			case 0:
			{
				clear();
				
				menusecundario(test);
				break;	
			}
                
			case 1:
			{	
				flag=true;
				clear();
				{ 	
					/*Carregar Grafo*/
					test.criarGrafo(map);
					cout <<endl;
				}
				pause();
				clear();
				break;
			}
                
			case 2:
			{
				clear();
				/*Visita em Largura - Iterativo*/
				
				string resposta;
				cout << "Deseja Fazer Visita em Largura por parametro: (s/n) ";
				
				cin >> resposta;
				
				if(resposta=="s" || resposta=="sim" || resposta== "SIM")
				{
				string local_origem;

				cout << "Insira Local de Origem: ";
				cin >>local_origem;

				Local *ci = new Local(local_origem); //local_origem aponta para objecto com descrição recebida

				map.visitaLargura(ci); // faz a visita em largura a partir do nó indicado

				}

				else if(resposta=="n" || resposta=="nao" || resposta== "NAO")
				{
				map.visitaLargura_root();
				}

				pause();
				break;			
			}
                
			case 3:
			{
				clear();
				/*Visita em Profundidade - Recursivo*/
				
				string resposta;
				cout << "Deseja Fazer Visita em Largura por parametro: (s/n) ";
				
				cin >> resposta;
				
				if(resposta=="s" || resposta=="sim" || resposta== "SIM")
				{
				
				string local_origem;

				cout << "Insira Local de Origem: ";
				cin >>local_origem;

				Local *ci = new Local(local_origem); //local_origem aponta para objecto com descrição recebida
				
				map.visitaProfundidade(ci); // faz a visita em profundidade a partir do nó indicado
				}

				else if(resposta=="n" || resposta=="nao" || resposta== "NAO")
				
				{
				map.visitaProfundidade_root();
				 
				}
				
				pause();
				break;		
				clear();
			}
                
			case 4:
			{
				/* Apresentar todos os percursos possíveis entre dois locais de interesse turístico */

				clear();

				cout <<"Apresentar todos os percursos possiveis";
					cout<<endl<<"entre dois locais de interesse turistico:"<<endl<<endl;

				string local_origem;
				string local_destino;
				
				
				cout << " Insira Local de Origem: ";
				cin >>local_origem;
				Local *ci = new Local(local_origem);
				
				cout << " Insira Local de Destino: ";
				cin >>local_destino;
				Local * cf = new Local(local_destino);

				cout<<endl<<endl;

				map.caminhosDistintos(ci,cf);
				
				pause();
				clear();
				break;			
			}
                
            case 5:
			{
				/* Apresentar o caminho mais curto entre dois pontos */
                
				clear();
                
				cout <<"Apresentar o percurso mais curto";
                cout<<endl<<"entre dois locais de interesse turistico:"<<endl<<endl;
                
				string local_origem;
				string local_destino;
				
				
				cout << " Insira Local de Origem: ";
				cin >>local_origem;
				Local *ci = new Local(local_origem);
				
				cout << " Insira Local de Destino: ";
				cin >>local_destino;
				Local * cf = new Local(local_destino);
                
				cout<<endl<<endl;
                
				map.menorCaminhoDistancia(ci,cf);
				
				pause();
				clear();
				break;			
			}
                
            case 6:
			{
				/* Apresentar o caminho mais económico entre dois pontos */
                
				clear();
                
				cout <<"Apresentar o percurso mais economico";
                cout<<endl<<"entre dois locais de interesse turistico:"<<endl<<endl;
                
				string local_origem;
				string local_destino;
				
				
				cout << " Insira Local de Origem: ";
				cin >>local_origem;
				Local *ci = new Local(local_origem);
				
				cout << " Insira Local de Destino: ";
				cin >>local_destino;
				Local * cf = new Local(local_destino);
                
				cout<<endl<<endl;
                map.menorCaminhoCusto(ci,cf);
				
				pause();
				clear();
				break;
			}
			case 7:
			{
				/*De maior interesse turístico (percurso que inclua o maior número de locais de interesse turístico)  */
                
				clear();
                
				cout <<"De maior interesse turistico";
                cout<<endl<<"(percurso que inclua o maior numero de locais de interesse turistico) "<<endl<<endl;
                
				string local_origem;
				string local_destino;
				
				
				cout << " Insira Local de Origem: ";
				cin >>local_origem;
				Local *ci = new Local(local_origem);
				
				cout << " Insira Local de Destino: ";
				cin >>local_destino;
				Local * cf = new Local(local_destino);
                
				cout<<endl<<endl;
                map.maiorInteresseTuristico(ci,cf);
				
				pause();
				clear();
				break;
			}
			default:
			{
				clear(); 
				cout << "Opcao invalida!" << endl << endl;
				
				pause();
				break;
			}
		}
	}while(opcao!=0);
}

void menusecundario(Teste &test){
	int opcao;
	do{
		cout << " Trabalho Pratico - ESINF 2013 \n" << endl;
		cout<<endl<<endl<<endl;
		cout << " |_______________ MENU______________"<<endl;
		cout << " 1: Listar Locais"<<endl;
		cout << " 2: Listar Vias"<<endl;
		cout << " 3: Grafo "<<endl;
		cout << " 0: Voltar ao Menu Principal "<<endl;
		cout << " |__________________________________|"<<endl;
		cin>>opcao;
		switch (opcao){
			case 0:
			{
				clear();
				
				menuprincipal();
				break;	
			}	
			case 1:
			{			
				clear();
				/*Lista Locais de interesse Historico e Natural*/
				test.listar_ordena_locais();

				pause();
				break;				
			}
			case 2:
			{
				clear();
				/*Lista Vias -> AutoEstrada e EstradaNacional*/
				test.listar_vecvias();

				pause();
				break;			
			}
			case 3:
			{
				clear();
				/*Grafo*/
				Mapa m;
				menusecundario2(test,m);
				
				pause();
				break;			
			}
			default:
			{
				clear(); 
				cout << "Opcao invalida!" << endl << endl;
				
				pause();
				break;
			}
		}
	}while(opcao!=0);
}

void menuprincipal(){
	bool flag=false;
	Teste test;
	int op;
	do{
		clear(); 
		cout << " Trabalho Pratico - ESINF 2013 \n" << endl<<endl;
		cout << " |_______________ MENU______________"<<endl;
		cout << " 1: Inserir Informacao a partir de Ficheiros (.txt) " <<endl;
		cout << " 2: Funcionalidades "<<endl;
		cout << " 3: Developers     "<<endl;
		cout << " 0: Sair " <<endl;
		cout << " |__________________________________|"<<endl;
		cin>>op;
		switch (op)
		{
			case 0:
				exit(1); 
			case 1: 
				flag=true;
				clear();
				{ 	
					test.LerFicheiro();
					cout <<endl;
				}
				pause();
				clear();
				break;

			case 2:
				clear();
				if (flag!=false)
					menusecundario(test);
				else{
					
					cout << "! Tem que carregar primeiro os ficheiros ( opcao 1 )"<<endl;
					cout << " , para aceder as Funcionalidades. "<<endl;
					cout << ""<<endl;			
				}
				pause();
				break;
			case 3:	
				{
				clear();
				cout << "_______________Developers:___________________"<<endl;
				cout << "                                           "<<endl;
				cout << "     Jose Faneco			1081344		"<<endl;	
				cout << "     Francisco Santos 			1111315     "<<endl;
				cout << "___________________________________________"<<endl;
				pause();
				break;				
				}
			default:
				{
				cout << "Opcao invalida!" << endl << endl;
				
				pause();
				
				}
			}
		
	}while(op!=0);
  
	pause();
};