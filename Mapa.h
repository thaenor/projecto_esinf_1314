#ifndef Mapa_
#define Mapa_

#include "graphStlPath.h"

#include "Local.h"
#include "PVia.h"

class Mapa :  public graphStlPath <Local*, PVia>
{// TV , TE
private:
	void mostraCaminho(int vi, vector <int> &path);
	void menorCaminho( Local *ci,  Local *cf);

	virtual bool compareVertices( Local* const &vContent1, Local* const &vContent2);

public:
	Mapa();
	Mapa(Mapa &m);

	void menorCaminhoDistancia( Local *ci,  Local *cf);
	void menorCaminhoCusto( Local *ci,  Local *cf);
	
	void visitaLargura(Local*ci); /* Visita em Largura a partir de um local */
	void visitaLargura_root();   /* Visita em Profundidade a partir da key inicial/no raiz do grafo */

	void visitaProfundidade(Local *ci); /* Visita em Profundidade a partir de um local */
    void visitaProfundidade_root();   /* Visita em Profundidade a partir da key inicial/no raiz do grafo */

	void caminhosDistintos(Local *ci, Local *cf);
	void maiorInteresseTuristico(Local* ci, Local* cf);
};

Mapa::Mapa() : graphStlPath <Local*, PVia>() {
}
Mapa::Mapa(Mapa &m) : graphStlPath <Local*, PVia>(m) {
}

bool Mapa::compareVertices( Local* const &vContent1, Local* const &vContent2)
{
	return (*vContent1==*vContent2);
}

//Usando M�todo de Disktra

/* mais curto em Kms  */
void Mapa::menorCaminhoDistancia( Local *ci,  Local *cf) {

	PVia::setComparacaoKMS();
	cout << "Menor distancia entre " << ci << " e " << cf << " : " ;
	menorCaminho(ci, cf);
}

/* mais econ�mico em Euros  */
void Mapa::menorCaminhoCusto( Local *ci,  Local *cf) {

	PVia::setComparacaoCUSTO();
	cout << "Menor custo entre " << ci << " e " << cf << " : ";
	menorCaminho(ci, cf);
}

void Mapa::menorCaminho(Local* ci, Local* cf) {
	vector <int> path; vector <PVia> dist; int key;
	
	this->getVertexKeyByContent(key, cf);
	this->dijkstrasAlgorithm(ci, path, dist);

	cout << dist[key].getCusto() << " EUR" << endl;
	cout << dist[key].getKM() << " KM " << endl;

	cout << ci;
	mostraCaminho(key, path);
}

void Mapa::mostraCaminho(int vi,  vector <int> &path) {
	if (path[vi]==-1) return;

	mostraCaminho(path[vi],  path);

	PVia e;	this->getEdgeByVertexKeys(e, path[vi], vi);
	Local *c; this->getVertexContentByKey(c, vi);
	cout << " -> " << e  << " -> " << c;
}
void Mapa::visitaLargura_root()
{
	/* Visita em largura, iterativo */ 

	Local *ci= new Local();
		
	if(getVertexContentByKey(ci,0) == true)
	
		breadthFirstVisit(ci);
	queue <Local*> q=breadthFirstVisit(ci);
		cout << "Local Origem: " << ci->getDescricao() << endl;
		cout << "Visita em largura: "<< endl<< endl;
		
	while (!q.empty())
	{
		cout << '<' << q.front() << '>'  << " ; ";
		q.pop();
	}
	cout <<endl<<endl;
}
void Mapa:: visitaLargura(Local *ci)
{
	/* Visita em largura, iterativo */ 

	queue <Local*> q=breadthFirstVisit(ci);
		cout << "Local Origem: " << ci->getDescricao() << endl;
		cout << "Visita em largura: "<< endl<< endl;
		
	while (!q.empty())
	{
		cout << '<' << q.front() << '>'  << " ; ";
		q.pop();
	}
	cout <<endl<<endl;

}
void Mapa::visitaProfundidade(Local *ci)
{
	/*  Visita em profundidade, recursivo */
		
        queue <Local*> q = lengthFirstVisit(ci);

            cout << "Local Origem: " << ci->getDescricao() << endl;
            cout << "Visita em profundidade: " << endl<< endl;
            while (!q.empty())
            {
                cout << '<' << q.front() << '>'  << " ; ";
                q.pop();
            }
       
    cout <<endl<<endl;
}

void Mapa::visitaProfundidade_root()
{
	/*  Visita em profundidade, recursivo */
	Local *ci= new Local();
    
	if(getVertexContentByKey(ci,0) == true)
	{
		queue <Local*> q = lengthFirstVisit(ci);
        cout << "Local Origem: " << ci->getDescricao() << endl;
        cout << "Visita em profundidade: " << endl<< endl;
        while (!q.empty())
        {
            cout << '<' << q.front() << '>'  << " ; ";
            q.pop();
        }
	}
    cout <<endl<<endl;
}
/*  Apresentar todos os percursos poss�veis entre dois locais de interesse tur�stico  */
void Mapa::caminhosDistintos(Local *ci, Local *cf)
{
 
	list<Local*>lista_Locais;

	queue < stack <Local*> > q = distinctPaths(ci,cf); //distinctPaths(const TV &vOrigin,const TV &vDestination)
	cout << "Caminhos entre " << ci << " e " << cf << " = " << q.size(); // Apresentado de forma invertida
	cout <<endl<< "Caminhos:" << endl<< endl;
	while (!q.empty())
	{
		stack <Local*> sp = q.front();
		while (!sp.empty()) {
			lista_Locais.push_front(sp.top());
			//cout << sp.top() << " <- ";
			sp.pop();
		}
		q.pop();
		cout << endl;
	}
	for (std::list<Local*>::iterator it = lista_Locais.begin(); it!=lista_Locais.end(); it ++)
	{
		cout << *it << " <- " <<endl;
	}
	
}
/*Maior interesse tur�stico (percurso que inclua o maior n�mero de locais de interesse tur�stico) */
void Mapa::maiorInteresseTuristico(Local* ci, Local* cf)
{
	queue <stack<Local*>> paths = distinctPaths(ci, cf);
	stack<Local*> s = paths.front();
	while (!paths.empty())
	{	
		if (s.size() < paths.front().size())
		{
			s = paths.front();
		}
		paths.pop();
	}
		while (!s.empty())
		{
			if (s.top()->getDescricao().compare(ci->getDescricao()) == 0)
			{
				cout << s.top()->getDescricao() << ".";
			}
			else {
				cout << s.top()->getDescricao() << " <- ";
			}
			s.pop();
		}
		cout << endl;
}

#endif