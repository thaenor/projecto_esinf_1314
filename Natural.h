#ifndef Natural_
#define Natural_

class Natural: public Local
{
private:
	double area; /* area do local em km^2 */

public:

	Natural(); /* Construtor por Defeito */
	Natural(const string descricao, double area);/* Construtor por parametros */
	Natural(const Natural&n);/* Construtor Copia */
	~Natural();/* Destrutor */

	void setArea(double area); /* M�todos Modificadores de Acesso (set) */	
	double getArea(); /* M�todos de Acesso (get) */
	
	void listar() const; /* M�todo Listar */
	
	Natural* clone() const;	/* Clone */

	void escreve(ostream &out) const;
	// => Sobrecarga de Operadores

		// Declara��o da sobrecarga do operador de atribui��o na classe Natural
	
		Natural & operator = ( const Natural &n);
};

/* Construtor por Defeito */
Natural::Natural():Local(){
    area = 0.0;
}

/* Construtor por parametros */
Natural::Natural(const string descricao, double area):Local(descricao){
	this->area=area;
}

/* Construtor Copia */
Natural::Natural(const Natural &n):Local(n){
	 this->area=n.area;
}

/* Destrutor */
Natural::~Natural()
{
}

/* M�todo Clone */
Natural* Natural::clone()const
{
    return new Natural(*this);
}

/* M�todos de Acesso (get) */
double Natural::getArea(){
    return area;
}
/* M�todos Modificadores de Acesso (set) */

void Natural::setArea(double a){
    area = a;
}
/* M�todo Listar */
void Natural::listar() const
{
	Local::listar();
	cout << "Area do local: " << area << " km^2"<<endl;
	
}
/* M�todo escreve */
void Natural::escreve(ostream &out) const
{
	Local::escreve(out);
	out <<  "Area do local: " << area <<" km^2"<< endl;	
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Natural &n){
	n.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Natural *n){
	n->escreve(out);
	return out;
}

// Implementa��o  do operador de atribui��o na classe Natural
Natural & Natural::operator = ( const Natural &n){
	if(this!=&n)
	{Local::operator=(n);
	area=n.area;
	}
	return *this;
}

#endif