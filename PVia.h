#ifndef PVia_
#define PVia_

// Mandatory:
// Constructor without parameters, copy constructor & destructor
// Operators: >, <, +, += e =
// The use of cloning is advised

class PVia {
private:
	Via *ap_via; 

	enum TipoComparacao { KMS , CUSTO }; 
	static TipoComparacao  tipoComparacao;

public:
	static void setComparacaoKMS();
	static void setComparacaoCUSTO();

	int getKM() const;
	double getCusto() const;

	PVia(); //default
	PVia(const Via &v); //copia de Via
	PVia(const PVia &v); //copia de apontador Via

	PVia(int km, double custo); //construtor por parametros Auto estrada
	PVia(int km); //construtor por parametros Estrada Nacional
	
	~PVia();

	//sobrecarga de operadores
	bool operator >(const PVia &v) const;
	bool operator <(const PVia &v) const;
	bool operator ==(const PVia &v) const;
	PVia operator+(const PVia &v);
	const PVia & operator+=(const PVia &v);
	const PVia & operator=(const PVia &v);

	void escreve(ostream &out) const;
};

PVia::TipoComparacao PVia::tipoComparacao=PVia::TipoComparacao::KMS;

void PVia::setComparacaoKMS() {
	tipoComparacao=TipoComparacao::KMS;
}
void PVia::setComparacaoCUSTO() {
	tipoComparacao=TipoComparacao::CUSTO;
}

int PVia::getKM() const {
	return ap_via->getTotal_km();
}

double PVia::getCusto() const {
	return ap_via->getCusto();
}

PVia::PVia() {
	this->ap_via = new AutoEstrada();
	//this->ap_via = new Via();
}

PVia::PVia(int km, double custo) { //construtor por parametros AutoEstrada
	Local l1;
	Local l2;
	ap_via = new AutoEstrada("",km,0,custo,l1,l2);
}

PVia::PVia(int km) { //construtor por parametros Estrada Nacional
	Local l1;
	Local l2;
	ap_via = new EstradaNacional("",km,0,"",l1,l2);
}

PVia::PVia(const Via &v) {
	this->ap_via = v.clone();
}
PVia::PVia(const PVia &v) {
	this->ap_via = v.ap_via->clone();
}
PVia::~PVia() {
	delete ap_via;
}

bool PVia::operator >(const PVia &v) const {	
	if (tipoComparacao==TipoComparacao::KMS) return this->ap_via->getTotal_km() > v.ap_via->getTotal_km();
	return this->ap_via->getCusto() > v.ap_via-> getCusto();
}
bool PVia::operator <(const PVia &v) const {
	if (tipoComparacao==TipoComparacao::KMS) return this->ap_via->getTotal_km() < v.ap_via->getTotal_km();
	return this->ap_via->getCusto() < v.ap_via->getCusto();
}
bool PVia::operator ==(const PVia &v) const {
	if (tipoComparacao==TipoComparacao::KMS) return this->ap_via->getTotal_km() == v.ap_via->getTotal_km();
	return this->ap_via->getCusto() == v.ap_via->getCusto();
}

PVia PVia::operator+(const PVia &v) {
	return PVia(this->ap_via->getTotal_km()+v.ap_via->getTotal_km(), this->ap_via->getCusto()+v.ap_via->getCusto());
}

const PVia & PVia::operator+=(const PVia &v) {
	this->ap_via->setTotal_km(this->ap_via->getTotal_km()+v.ap_via->getTotal_km());
	if (typeid(*ap_via)==typeid(AutoEstrada)) {
		AutoEstrada *ae = (AutoEstrada *)this->ap_via;
		ae->setPortagem(ae->getCusto()+v.ap_via->getCusto());
	}	
	return  *this;
}
const PVia & PVia::operator=(const PVia &v) {
	this->ap_via = v.ap_via->clone();
	return *this;
}
void PVia::escreve(ostream &out) const {
	out << *ap_via;
}

/* Operador de sobrecarga << ostream out */
ostream &operator <<(ostream &out, const PVia &v)
{
	v.escreve(out);
	return out;
}

#endif