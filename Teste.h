﻿#ifndef Teste_ 
#define Teste_

#include "graphStlPath.h"

class Teste
{

private:
	vector<Local*> locais_vec;
	vector<Via*> vias_vec;
	int tam;
public: 
	Teste();/* Construtor por defeito */
    Teste(int tam); /* Construtor com parametro */
	Teste(const Teste &test);/* Construtor Copia */
    ~Teste(); /* Destrutor */
	
	// Declaração da sobrecarga do operador stream out >>
	void escreve(ostream &out) const;

	// Declaração do operador de atribuição na classe Teste
	Teste & operator = ( const Teste &test);

	//Métodos e Funções

	void LerFicheiro();

    void carregarFicheirosLocais();
	void carregarFicheirosVias();

	void listar_veclocais();
	void listar_vecvias();

	void listar_ordena_locais();

	void ordenar_objectos(list <string> listLoc,vector<Local*> locais_vec_aux);

	Local getLocal(string local);

	// PARTE 2 - Projecto de ESINF 2013 / 2014
	
	void criarGrafo(Mapa &m); // Método Carrega Grafo (Bidirecional)
}; 


void Teste::LerFicheiro(){
	carregarFicheirosLocais();
	carregarFicheirosVias();
}
/* Construtor por Defeito */
Teste::Teste() {
	tam=100;
};

Teste::Teste(int t){
tam=t;
}

/* Construtor Copia */
Teste::Teste(const Teste& test){

    locais_vec = test.locais_vec;
    vias_vec = test.vias_vec;

}
/* Destrutor */ 
Teste::~Teste() {
    locais_vec.clear();
	vias_vec.clear();
};
  
/* Importar Ficheiros */
  
//Obtencao e atribuicao de dados a partir da leitura de Ficheiro de texto 
//descricao ,area -> Naturais 
//descricao ,tempo_medio ,horario_abertura, horario_encerramento -> Historicos 
  
void Teste::carregarFicheirosLocais()
{ 
    ifstream origem; // origem -> ficheiro de entrada 
    origem.open("locais.txt"); // define variavel e abre ficheiro para leitura 
    if (!origem){ 
        cerr<<"Erro a abrir ficheiro locais.txt !!!"<<endl;
    }else{ 
        string linha; 
  
        string descricao, tempo_visita, horario_abertura, horario_encerramento,area; 
        int n_parametros=0; // Se tiver >2 Historico ; Senao Natural
  
        while(!origem.eof()) 
        { 
            getline(origem, linha, '\n'); 
  
                  
            if(linha.size() > 0) 
            {    
                int inic=0; 
                int pos=linha.find(',', inic); // encontra a primeira palavra ate а primeira virgula 
                descricao=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
            //  cout << descricao << endl; 
  
                inic = pos; 
                pos=linha.find(',', inic);  
  
                if(pos == -1)   //se não encontrar virgula a 2ª vez, o local é Natural e terá que obter a palavra
                { 
                    area = linha.substr(inic,(linha.size()-1)); //(linha.size()-1 é a posição do ultimo caracter da ultima palavra)
					n_parametros++; 

                }else
                { 
                    tempo_visita=linha.substr(inic,pos-inic); 
                    pos++; 
                    n_parametros++; 
                    //  cout << tempo_visita << endl; 

                    inic = pos; 
                    pos=linha.find(',', inic);  
                    horario_abertura=linha.substr(inic,pos-inic); 
                    pos++; 
                    n_parametros++; 
                //  cout << horario_abertura << endl; 
                      
                    inic = pos; 
                    pos=linha.find(',', inic); 
                    horario_encerramento=linha.substr(inic,pos-inic); 
                    pos++; 
                    n_parametros++; 
                //  cout << horario_encerramento << endl; 
                } 
  
            } 
		//	cout << "--------------------------------------------------- "<<endl;
                if(n_parametros==4){ 
  
                    Historico *h = new Historico(); 
                     
                    h->setDescricao(descricao);

                    h->setTempoVisita(atoi(tempo_visita.c_str()));
                    h->setHorario_abertura(atoi(horario_abertura.c_str())); 
                    h->setHorario_encerramento(atoi(horario_encerramento.c_str()));

					locais_vec.push_back(h->clone());

                //   cout << "OBJECTO LOCAL HISTORICO ADICIONADO: => " << descricao <<"\n\n" << h <<endl; 

                } 
  
                if(n_parametros==2 ) 
                {  
					
                   Natural *n = new Natural();

                   n->setDescricao(descricao); 
                   n->setArea(atof(area.c_str()));

					locais_vec.push_back(n->clone());

				//	cout << "OBJECTO LOCAL NATURAL ADICIONADO: => " << descricao <<"\n\n" << n <<endl;  
                }
                n_parametros=0; 
        }
        cout<<"Ficheiro locais.txt Carregado com Sucesso...\n"<<endl;
    } 
	
    origem.close();
}

void Teste::listar_veclocais(){
	cout << " LISTAGEM DO VECTOR LOCAIS " <<endl<<endl;
	
	int tamanho_locais_vec=locais_vec.size();
	for(int i=0; i<tamanho_locais_vec; i++)
		cout <<"VECTOR POSICAO: " << i <<"\n" <<  locais_vec[i]<<"\n" <<endl<<endl;
}

void Teste::listar_ordena_locais()
{
	int cont_locais=0;
	int cont_locais_historico=0;
	int cont_locais_natural=0;

	vector<Local*> locais_vec_aux=locais_vec;

	list <string> lista_descricao_h;
	list <string> lista_descricao_n;
	list <string> lista_descricao;

	int i;
	int tamanho_locais_vec=locais_vec.size();
	for(i=0; i<tamanho_locais_vec; i++){
		cont_locais++;

		lista_descricao.push_front(locais_vec[i]->getDescricao());
		
		Historico* pointer_h = dynamic_cast<Historico*>(locais_vec[i]);
		if(pointer_h)
		{
			cont_locais_historico++;

			
			lista_descricao_h.push_front(pointer_h->getDescricao());
			//cout <<"VECTOR POSICAO: " << i <<"\n" <<  locais_vec[i]<<"\n" <<endl<<endl;
		}

		Natural* pointer_n= dynamic_cast<Natural*>(locais_vec[i]);

		if(pointer_n)
		{
			cont_locais_natural++;
			
			lista_descricao_n.push_front(pointer_n->getDescricao());
			//cout <<"VECTOR POSICAO: " << i <<"\n" <<  locais_vec[i]<<"\n" <<endl<<endl;			
		}
	
	}
	cout <<endl<< "Lista Ordenada de Locais Historicos pela descricao: " <<endl<<endl;

	lista_descricao_h.sort(); /* Ordenar */
	
		for (string l: lista_descricao_h){
			//cout <<"Local Historico:" <<endl<<endl;
			lista_descricao_h.push_front(l);
			cout << l <<endl;
		}

		cout <<endl<< "Lista Ordenada de Locais Naturais pela descricao:" <<endl<<endl;

		lista_descricao_n.sort(); /* Ordenar */
	
		for (string l: lista_descricao_n)
		{
			//cout <<"Local Natural:" <<endl<<endl;
			lista_descricao_n.push_front(l);
			cout << l <<endl;
		}

	cout <<endl<< " Quantidade de Locais:   "<<endl<<endl;
	cout << " Locais Turisticos = " << cont_locais <<endl;
	cout << " Locais Turisticos Naturais = " << cont_locais_natural <<endl;
	cout << " Locais Turisticos Historicos = " << cont_locais_historico <<endl<<endl;

	cin.get();
	ordenar_objectos(lista_descricao,locais_vec_aux);
	
}

void Teste::ordenar_objectos(list <string> listLoc,vector<Local*> locais_vec_aux)
{
	//Local *l;
	string txt_l="";
	string txtlv="";
		cout <<endl<< "Vector Locais Turisticos Ordenado " <<endl<<endl;
		int tamanho_locais_vec_aux = locais_vec_aux.size();
		for(int i=0; i<tamanho_locais_vec_aux; i++)
		{
		
			txtlv=locais_vec_aux.front()->getDescricao();
		
			for (list <string> ::iterator it = listLoc.begin(); it != listLoc.end(); it++ )
			{
				txt_l=*it;
				//cout <<"ITERATOR: "<< *it  << endl;
				if (txt_l.compare(txtlv)==1)
				{
				}
			}cout << "POSICAO " << i << ":\n" <<locais_vec_aux[i] <<endl;
		}
}

//Obtencao e atribuicao de dados a partir da leitura do ficheiro de texto vias.txt
//const Local &local_origem,const Local &local_destino
//const string codigo, int total_km, int tempo_medio, double portagem -> AutoEstrada
//const string codigo, int total_km, int tempo_medio, string tipo_pavimento ->EstradaNacional
void Teste::carregarFicheirosVias()
{ 
    ifstream origem; // origem -> ficheiro de entrada
    origem.open("vias.txt"); // define variavel e abre ficheiro para leitura
    //system("pwd");
    if (!origem){ 
        cerr<<"Erro a abrir ficheiro ficheiro vias.txt \n"<<endl;

    }else{ 
        string linha; 
  
        string local_origem, local_destino, codigo, total_km,tempo_medio,portagem; 
        int n_parametros=0; // Se tiver >2 Historico ; Senao Natural
  
        while(!origem.eof()) 
        { 
            getline(origem, linha, '\n'); 
  
            if(linha.size() > 0) 
            {    
                int inic=0; 
                int pos=linha.find(',', inic); // encontra a primeira palavra ate а primeira virgula 
                local_origem=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
              
				//cout << local_origem << endl; 
  
                inic = pos; 
                pos=linha.find(',', inic); 
                local_destino=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
                
				//cout << local_destino << endl; 
  
                inic = pos; 
                pos=linha.find(',', inic);  
                codigo=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
                 
				//cout << codigo << endl; 
                      
                inic = pos; 
                pos=linha.find(',', inic); 
                total_km=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
             
				//cout << total_km << endl;

				inic = pos; 
                pos=linha.find(',', inic); 
                tempo_medio=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
               
				//cout << tempo_medio << endl; 

				inic = pos; 
                pos=linha.find(',', inic); 
                portagem=linha.substr(inic,pos-inic); 
                pos++; 
                n_parametros++; 
               
				//cout << portagem << endl; 

                }
		//		cout << "--------------------------------------------------- "<<endl;
                if(codigo.length()==3){
					
					EstradaNacional *en = new EstradaNacional(); 
					
					//---------------
					en->setLocalOrigem(local_origem);
					en->setLocalDestino(local_destino);

					en->setCodigo(codigo); 
					en->setTotal_km(atoi(total_km.c_str())); 
					en->setTempo_medio(atoi(tempo_medio.c_str())); 
					en->setTipo_pavimento(portagem);

					
					
					vias_vec.push_back(en->clone());

				//	cout << "OBJECTO VIA ADICIONADO: => EstradaNacional " << codigo <<"\n\n" << en <<endl; 
                }

				if(codigo.length()==2){
					
					AutoEstrada *a = new AutoEstrada();

					a->setLocalOrigem(local_origem);
					a->setLocalDestino(local_destino);

					a->setCodigo(codigo); 
					a->setTotal_km(atoi(total_km.c_str())); 
					a->setTempo_medio(atoi(tempo_medio.c_str()));
					
					a->setPortagem(atof(portagem.c_str()));

					vias_vec.push_back(a->clone());

               //    cout << "OBJECTO VIA ADICIONADO: => Autoestrada " << codigo <<"\n\n" << a <<endl;
					
				}
                n_parametros=0;
        }
        cout<<"Ficheiro vias.txt Carregado com Sucesso..."<<endl;
    } 
    origem.close();
}

void Teste::listar_vecvias(){
	cout << " LISTAGEM DO VECTOR VIAS " <<endl<<endl;
	int tam_vias_vec = vias_vec.size();
	for(int i=0; i<tam_vias_vec; i++)
    {
		cout <<"VECTOR POSICAO: " << i <<"\n" <<  vias_vec[i]<<"\n" <<endl<<endl;

    }
}

//Implementação da sobrecarga do operador stream out >>
void Teste::escreve(ostream &out) const{

	out << "Lista de " << tam << " locais: " << endl;
	for (int i=0; i<tam; i++)
		out << *locais_vec[i];
}
// Método Global definido na classe, mas nao associado à classe.
ostream & operator<<(ostream &out, const Teste &test){
	test.escreve(out);
	return out;
}

// Implementação do operador de atribuição na classe Teste
Teste & Teste::operator = ( const Teste &test){
	if(this!=&test)
	{
		for(int i=0; i<tam; i++) delete locais_vec[i];
		tam = test.tam;
		for( int i=0; i<tam; i++) locais_vec[i] = test.locais_vec[i]-> clone();
	}
return *this;
}


void Teste::criarGrafo(Mapa &m)
{
	
	cout << " Carregamento do Grafo: " <<endl<<endl;
	
	int tamanho_vias_vec = vias_vec.size();

	for (int i=0; i<tamanho_vias_vec; i++ ) 
	{
		
		if(typeid(*vias_vec[i])==typeid(AutoEstrada))
		{
			
			PVia ap_via(AutoEstrada(vias_vec[i]->getCodigo(),
									vias_vec[i]->getTotal_km(),
									vias_vec[i]->getTempo_medio(),
									vias_vec[i]->getCusto(),
									vias_vec[i]->getLocalOrigem(),
									vias_vec[i]->getLocalDestino())
									);

			Local *lo= new Local(vias_vec[i]->getLocalOrigem());
			Local *ld= new Local(vias_vec[i]->getLocalDestino());
			
			Local * ap_origem = lo;
			Local * ap_destino = ld;
				
			int dimensao_vec_locais = locais_vec.size();

			for (int i = 0; i < dimensao_vec_locais; i++)
			{
				if(*locais_vec[i]==*ap_origem)
				{
					ap_origem=locais_vec[i];
				}

				if(*locais_vec[i]==*ap_destino)
				{
					ap_destino=locais_vec[i];
				}
			}


			m.addGraphEdge(ap_via, ap_origem,ap_destino);
			m.addGraphEdge(ap_via, ap_destino,ap_origem);
		}
	
		if(typeid(*vias_vec[i])==typeid(EstradaNacional))
		{

			PVia ap_via(EstradaNacional(vias_vec[i]->getCodigo(),
										vias_vec[i]->getTotal_km(),
										vias_vec[i]->getTempo_medio(),
										vias_vec[i]->getTipo_pavimento(),
										vias_vec[i]->getLocalOrigem(),
										vias_vec[i]->getLocalDestino()));


			Local *lo= new Local(vias_vec[i]->getLocalOrigem());
			Local *ld= new Local(vias_vec[i]->getLocalDestino());

			Local * ap_origem = lo;
			Local * ap_destino = ld;

			int dimensao_vec_locais = locais_vec.size();

			for (int i = 0; i < dimensao_vec_locais; i++)
			{
				if(*locais_vec[i]==*ap_origem)
				{
					ap_origem=locais_vec[i];
				}

				if(*locais_vec[i]==*ap_destino)
				{
					ap_destino=locais_vec[i];
				}
			}

			m.addGraphEdge(ap_via, ap_origem,ap_destino);
			m.addGraphEdge(ap_via, ap_destino,ap_origem);

		}
	}
		cout << m << endl << endl;
}

#endif
