#ifndef Via_
#define Via_

class Via /* SuperClasse */
{
private:

	string codigo; /* Codigo da via de liga��o */
	int total_km; /* Total de kilometros da via */
	int tempo_medio; /* Tempo M�dio do percurso em minutos entre um local origem e um local destino */
	
	Local local_origem; /* Local de Origem */
	Local local_destino; /* Local de Destino */
	
public:
	
	Via(); /* Construtor por Defeito */

	//Via(const string codigo, int total_km, int tempo_medio); /* Construtor com parametros */
	Via(const string codigo, int total_km, int tempo_medio, const Local &local_origem, const Local &local_destino); /* Construtor com parametros */
	Via(const Via &v); /* Construtor copia */
	virtual ~Via(); /* Destrutor */ 

	/* M�todos Modificadores de Acesso (set) */
		
		void setCodigo(string codigo);
		void setTotal_km(int total_km);
		void setTempo_medio(int tempo_medio);

		void setLocalOrigem(const Local&local_origem_);
		void setLocalDestino(const Local&local_destino_);

	/* M�todos de Acesso (get) */

		string getCodigo() const;
		int getTotal_km();
		int getTempo_medio();

		Local getLocalOrigem() const;
		Local getLocalDestino() const;


		virtual double getCusto()const;
		virtual string getTipo_pavimento();

	/* M�todos Virtuais puros */

		virtual Via* clone()const; /* Clone */
		virtual void listar() const;/* M�todo para Listar Conteudo de uma Via */

// => Sobrecarga de Operadores

	// Declara��o da sobrecarga do operador stream out >>
		virtual void escreve(ostream &out) const;

	// Declara��o da sobrecarga do operador de atribui��o na classe Via
		virtual Via & operator = (const Via &v);

	// Declara��o da sobrecarga do operador > que permita comparar o codigo de duas vias.
		bool operator > ( const Via &v) const; // Compara��o pelo Codigo do Via

	// Declara��o da sobrecarga  do operador == que compare o codigo de duas vias.
		
		bool operator==(const Via& v) const;

		
};

/* Construtor por Defeito */
Via::Via():local_origem(),local_destino()
{
	codigo = "";
    total_km = 0;
	tempo_medio=0;
}

Via::Via(const string codigo, int total_km, int tempo_medio, const Local &local_origem, const Local &local_destino)
{
	this->codigo=codigo;
	this->total_km=total_km;
	this->tempo_medio=tempo_medio;
	this->local_origem=local_origem;
	this->local_destino=local_destino;
}

/* Construtor copia */
Via::Via(const Via& v):local_origem(),local_destino()
{
	this->codigo=v.codigo;
	this->total_km=v.total_km;
	this->tempo_medio=v.tempo_medio;
	this->local_origem=v.local_origem;
	this->local_destino=v.local_destino;
}

/* Destrutor */ 
Via::~Via() {

}

/* M�todos Modificadores de Acesso (set) */

void Via::setCodigo(string cod)  {
    codigo = cod;
}

void Via::setTotal_km(int km)  {
    total_km = km;
}

void Via::setTempo_medio(int tempo)  {
    tempo_medio = tempo;
}

void Via::setLocalOrigem(const Local&l)
{
	local_origem=l;
//local_origem.setDescricao(l.getDescricao());
}

void Via::setLocalDestino(const Local&l)
{
	local_destino=l;
	//local_destino.setDescricao(l.getDescricao());
}


/* M�todos de Acesso (get) */

string Via::getCodigo() const {
    return codigo;
}

int Via::getTotal_km() {
    return total_km;
}

int Via::getTempo_medio()  {
    return tempo_medio;
}

Local Via::getLocalOrigem() const
{
	return local_origem;
}

Local Via::getLocalDestino() const
{
	return local_destino;
}

double Via::getCusto()const{
    return 0;
}

string Via::getTipo_pavimento(){
	return "";
}

/* M�todo Clone */

Via* Via::clone()const
{
    return new Via(*this);
}

/* M�todo Listar -> Lista Conteudo do Objecto da classe Via*/
void Via::listar() const
{
	cout<<endl;
	cout << "Codigo da via de ligacao: " << codigo << endl;
	cout << "Total de kilometros da via: " << total_km << " km"<<endl;
	cout << "Tempo Medio do percurso: " << tempo_medio << " min"<<endl;
	cout << "Origem => ";
	local_origem.listar();
	cout << "Destino => ";
	local_destino.listar();
	cout<<endl;
}
void Via::escreve(ostream &out) const
{
	out<<endl;
	out << "Codigo da via de ligacao: " << codigo << endl;
	out << "Total de kilometros da via: " << total_km << " km"<<endl;
	out << "Tempo Medio do percurso: " << tempo_medio << " min"<<endl;
	out << "Origem => ";
	local_origem.escreve(out);
	out << "Destino => ";
	local_destino.escreve(out);
}



// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Via &v){
	v.escreve(out);
	return out;
}

// M�todo Global definido na classe, mas nao associado � classe.
ostream& operator<<(ostream& out, const Via *v){
	v->escreve(out);
	return out;
}

// Implementa��o da sobrecarga  do operador == que compare o codigo de duas Vias.

bool Via::operator==(const Via& v) const
{
	if (codigo == v.codigo)
		return true;
	else
		return false;
}


// Implementa��o da sobrecarga do operador de atribui��o na classe Via
Via & Via::operator = (const Via &v){
	if(&v!=this){
		/*local_origem=v.local_origem;
		local_destino=v.local_destino;*/
		setCodigo(v.codigo);
	}
	return *this;
}

// Implementa��o da sobrecarga do operador > que permita comparar o codigo de duas Vias.
bool Via::operator > ( const Via &v) const{
	return (codigo > v.codigo);
}

#endif